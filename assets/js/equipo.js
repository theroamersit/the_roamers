/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var slideIndex = 0;

//get a all the slides and the textslides into arrays
var photoslides = document.getElementsByClassName('imageholder');
var textslides = document.getElementsByClassName('description');

//creating a dot for each slide we have
createScrollDots();
//get all the dots into arrays after creating them
var dots = document.getElementsByClassName('dot');

//calling the function to set up the first slide
showSlides(slideIndex);

//setting the event listener whne the wheel of the mouse is used
window.addEventListener("wheel", event => {
    const delta = Math.sign(event.wheelDelta);
    //calling a function and passing through the +1 or -1 if the wheel goes
    //up or down respectively
    plusSlides(-delta);
});

//sets the sroll to the top when the width of the window is superior to
//660px for responsive porpouses
window.addEventListener("resize", function(){
    if (window.innerWidth > 660){
        window.scrollTo(0, 0);
    }
});

function plusSlides(n) {
    //adds the +1 or -1 to the slideIndex so we can move through them
    showSlides(slideIndex += n);
}

//shows the slide that is passed through as parameter
function showSlides(n) {
    // if it reaches the top slide slideIndex doesn't get incremented so it
    // keeps the last slide on
    if (n > photoslides.length) {
        slideIndex = photoslides.length;
    } 
    // if it reaches the bottom slide it doesn't get decreased so it keeps
    // the first slide on
    if (n < 1) {
        slideIndex = 1;
    }
    // set all the slides out of the view and off the dots
    for (let i = 0; i < photoslides.length; i++) {
        photoslides[i].className = photoslides[i].className.replace(" slideactive", "");
        textslides[i].className = textslides[i].className.replace(" descriptionactive", "");
        dots[i].className = dots[i].className.replace(" dotsactive", "");
    }   
    // sets the slide that has to be displayed on the window and on the dot corresponding
    // to the slide displayed
    photoslides[slideIndex-1].className += " slideactive";
    textslides[slideIndex-1].className += " descriptionactive";
    dots[slideIndex-1].className += " dotsactive";
    
    console.log('slide #: ' + n); //TO BE DELETED
}

//creates the dots to navigate on the slides
function createScrollDots(){
    for (let j = 0; j < photoslides.length; j++){
        //get the container of the dots (<ul id="multiscroll-nav">) that will scroll 
        //through the slides and add to them as many <li> tags as slides we have. 
        //After that an individual class will be added to them
        document.getElementById('multiscroll-nav-column').
                appendChild(document.createElement("li")).
                classList.add("scroll" + (j+1));
        //adding the class dot to the <li> and also the onclick event that will
        //show the corresponding slides when clicked
        var t = document.getElementsByClassName("scroll" + (j+1));
        t[0].classList.add('dot');
        t[0].onclick = function() {
            slideIndex = j+1;
            showSlides(j+1);
        };
    }
}