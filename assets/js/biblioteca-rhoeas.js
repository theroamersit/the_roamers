/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$('.cell_holder').hide();
$('.manual_cell_holder').hide();

// booleans used to check if the scrolls are open or not
var manualActive = false, shortstoriesActive = false;

//Function to open ShortStories
$('#manual').click(function() {
    
    if (manualActive === false){
        $('.manual_cell_holder').show("slow");
        manualActive = true;
    } else {
        $('.manual_cell_holder').hide("slow");
        manualActive = false;
    }
});

//Function to open ShortStories
$('#shortstories').click(function() {
    
    if (shortstoriesActive === false){
        $('.cell_holder').show("slow");
        shortstoriesActive = true;
    } else {
        $('.cell_holder').hide("slow");
        shortstoriesActive = false;
    }
});

