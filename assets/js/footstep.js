jQuery(function (){
    
    var mouse_x = 0, mouse_y = 0, px = 0, py = 0, previus_mouse_x = 0, previus_mouse_y = 0, i=0, tamX = 78, tamY = 125;
    var rightFoot = true;
    
    $(document).on('mousemove', function( event ) {
        //save the current mouse position
	mouse_x = event.pageX;
  	mouse_y = event.pageY;
        //Check if the mouse have moved enough, if so, call the function
	if((!(previus_mouse_x === mouse_x) || !(previus_mouse_y === mouse_y)) && (Math.abs(previus_mouse_x - mouse_x)>50 || Math.abs(previus_mouse_y - mouse_y)>50)){
            moveFeet();
	}
    });
    
    function moveFeet(){
    	
        //Calculate rotation angle
        var radians = Math.atan2(mouse_x - previus_mouse_x, mouse_y - previus_mouse_y);
        var angleDeg = (radians * (180 / Math.PI) * -1 ) + 90;
        
        px += (mouse_x - px - tamX);
	py += (mouse_y - py - tamY);
        
	//switching between right and left foot//adding the div with the footstep to the html
	if (rightFoot){
            $('<div id="'+ i +'" class="rfoot"></div>').appendTo($('footer'));
            rightFoot = false;
	} else {
            $('<div id="'+ i +'" class="lfoot"></div>').appendTo($('footer'));
            rightFoot = true;
	}
        //adding the div with the footstep to the html
        $('#' + i).css({"pointer-events": 'none', "top": py + 'px', "left": px + 'px', 'transform': 'rotate(' + angleDeg + 'deg) scale(0.25,0.25)'});
        $('#' + i).fadeOut(800);
        
	var j = i-20;
        
	$('#' + j).remove();
	
	i++; // i should be changed
        //save the mouse position to compare it with the next one
	previus_mouse_x = mouse_x;
	previus_mouse_y = mouse_y;
    }
    
    mouseIsMoving = false;
    isScrolling = false;
});